import React from 'react';
import './App.css';
import Navbar from '../src/components/navbar';
import Footer from '../src/components/footer';

function App() {
  return (
    <div>
      <Navbar />
      <Footer />
    </div>
  );
}

export default App;
