import React from 'react';
import '../styles/store.scss';

export default class Store extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: '' };
  }
  componentDidMount() {
    let currentPage = 1;
    let totalPages;
    const numOfItems = 6;

    let template = document.getElementById('template-list-item');
    let templateHtml = template.innerHTML;
    let listHtml = '';
    let cartData;
    fetch('./data/page1.json', {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(function(resp) {
        return resp.json();
      })
      .then(data => {
        listHtml = fillTemplate(data);
        document.getElementById('store-items').innerHTML = listHtml;
        totalPages = data.meta.totalPages;
        this.setState({ items: this.state.items + data });
        onClickAction();
      });

    this.scrollHandler = () => {
      let additionalHtml = '';
      let block = document.getElementById('store-items');
      let contentHeight = block.offsetHeight;
      let yOffset = window.pageYOffset;
      let windowHeight = window.innerHeight;
      let y = yOffset + windowHeight;

      if (y >= contentHeight) {
        currentPage = currentPage + 1;

        if (currentPage <= totalPages) {
          fetch('./data/page' + currentPage + '.json', {
            headers: {
              'Content-Type': 'application/json',
              Accept: 'application/json',
            },
          })
            .then(function(resp) {
              return resp.json();
            })
            .then(data => {
              additionalHtml = '';
              additionalHtml = fillTemplate(data);
              document.getElementById(
                'store-items',
              ).innerHTML += additionalHtml;
              totalPages = data.meta.totalPages;
              this.setState({ items: this.state.items + data });
              onClickAction();
            });
        }
      }
    };

    window.addEventListener('scroll', this.scrollHandler);

    function fillTemplate(data) {
      let newTemplate = '';
      for (let key = 0; key < numOfItems; key++) {
        let temp = templateHtml
          .replace(/#id#/g, data.items[key]['id'])
          .replace(/#subtitle#/g, data.items[key]['subtitle'])
          .replace(/#image#/g, data.items[key]['image'])
          .replace(/#name#/g, data.items[key]['name'])
          .replace(/#price#/g, data.items[key]['price']);
        newTemplate += temp;
      }
      return newTemplate;
    }

    document.getElementById('myInput').onkeyup = function storeSearch() {
      var input, filter, itemsBlock, item, name, i;
      input = document.getElementById('myInput');
      filter = input.value.toUpperCase();
      itemsBlock = document.getElementById('store-items');
      item = itemsBlock.getElementsByClassName('blog-article-card');

      for (i = 0; i < item.length; i++) {
        name = item[i].getElementsByTagName('p')[0];
        if (name.innerHTML.toUpperCase().indexOf(filter) > -1) {
          item[i].style.display = '';
        } else {
          item[i].style.display = 'none';
        }
      }
    };

    let addItemToCart = name => {
      let title = name.querySelector('.store-text');
      let itemId = title.getAttribute('data-id');
      cartData = JSON.parse(localStorage.getItem('cart')) || [];
      cartData.push(itemId);
      alert('Товар: ' + title.innerHTML + ' добавлен в корзину');
      localStorage.setItem('cart', JSON.stringify(cartData));
      this.props.updateItemsNumber(cartData.length);
    };

    let onClickAction = () => {
      let links = document.getElementsByClassName('cardInfoLink');
      Array.from(links).forEach(element => {
        element.onclick = () => addItemToCart(element);
      });
    };

    let spinner = document.querySelector('#loading');
    spinner.style.visibility = 'hidden';
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollHandler);
  }
  render() {
    return (
      <div className="blog">
        <div className="page-wrapper">
          <div className="main-wrapper">
            <div id="loading">
              <img
                id="loading-image"
                src="img/ajax-loader.gif"
                alt="Loading..."
              />
            </div>
            <section className="blog-arcticles">
              <div className="container">
                <div className="blog-top-panel">
                  <h2 className="title">Store</h2>
                  <input
                    className="blog-search"
                    id="myInput"
                    placeholder="Search"
                  />
                </div>
                <div className="blog-articles-block" id="store-items" />
              </div>
            </section>
          </div>
        </div>
        <script id="template-list-item" type="text/template">
          <div className="blog-article-card">
            <div href="/#" className="cardInfoLink" id="cardInfo">
              <div className="store-subtitle" id="subtitle">
                #subtitle#
              </div>
              <img className="item-image" src="#image#" alt="item" />
              <p className="store-text" data-id="#id#">
                #name#
              </p>
              <div className="store-subtitle" id="price">
                $ #price#
              </div>
            </div>
          </div>
        </script>
      </div>
    );
  }
}
