import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import '../styles/styles.scss';
import Home from './home';
import Blog from './blog';
import Checkout from './checkout';
import Store from './store';
import { faShoppingCart, faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { numberItems: 0 };
  }
  componentDidMount() {
    let mainNav = document.getElementById('js-menu');
    let navBarToggle = document.getElementById('js-navbar-toggle');
    let head = document.getElementById('header-js');
    let navLi = document.getElementById('nav-li');
    let logo = document.getElementById('logo');

    navBarToggle.addEventListener('click', function() {
      mainNav.classList.toggle('active');
      head.classList.toggle('white-header-mobile');
      navLi.classList.toggle('mobile-nav');
      logo.classList.toggle('hide-element');
    });
  }

  updateItemsNumber = number => this.setState({ numberItems: number });

  render() {
    return (
      <Router>
        <header className="white-header" id="header-js">
          <div className="container">
            <div className="header-content">
              <span className="navbar-toggle" id="js-navbar-toggle">
                <FontAwesomeIcon icon={faBars} size="2x" />
              </span>
              <Link className="logo" to={'/'} id="logo">
                <span className="logo-icon" />
                project name
              </Link>
              <div href="/#" id="mobile-cart">
                <div id="ex2">
                  <span
                    className="fa-stack fa-2x has-badge"
                    data-count={this.state.numberItems}
                  >
                    <Link to={'/checkout'}>
                      <FontAwesomeIcon icon={faShoppingCart} size="1x" />
                    </Link>
                  </span>
                </div>
              </div>
              <nav id="js-menu">
                <ul>
                  <li id="nav-li">
                    <Link to={'/store'} className="link">
                      STORE
                    </Link>
                  </li>
                  <li id="nav-li">
                    <a href="/#" className="link">
                      CONSULTING
                    </a>
                  </li>
                  <li id="nav-li">
                    <a href="/#" className="link">
                      ABOUT
                    </a>
                  </li>
                  <li id="nav-li">
                    <Link to={'/blog'} className="link">
                      BLOG
                    </Link>
                  </li>
                  <li id="nav-li">
                    <div id="ex2">
                      <span
                        className="fa-stack fa-2x has-badge"
                        data-count={
                          localStorage.getItem('cart')
                            ? JSON.parse(localStorage.getItem('cart')).length
                            : 0
                        }
                      >
                        <Link to={'/checkout'}>
                          <FontAwesomeIcon icon={faShoppingCart} size="1x" />
                        </Link>
                      </span>
                    </div>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </header>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/blog" component={Blog} />
          <Route
            path="/store"
            component={() => (
              <Store updateItemsNumber={this.updateItemsNumber} />
            )}
          />
          <Route path="/checkout" component={Checkout} />
        </Switch>
      </Router>
    );
  }
}
