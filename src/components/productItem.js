import React from 'react';

export default class ProductItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = { quantity: 1 };
  }

  render() {
    const { product } = this.props;
    let updateNewProducts = Cart => {
      this.props.updateProducts(Cart);
    };
    function handleClick(e) {
      e.preventDefault();
      let listItems = JSON.parse(localStorage.getItem('cart')) || [];

      listItems = listItems.filter(item => item !== product.id);

      localStorage.setItem('cart', JSON.stringify(listItems));

      let Cart = [];

      fetch('./data/allPages.json', {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      })
        .then(function(resp) {
          return resp.json();
        })
        .then(data => {
          data.items.forEach(item => {
            for (var purchaseId in listItems) {
              if (item.id === listItems[purchaseId]) {
                Cart.push(item);
              }
            }
          });
        });
      updateNewProducts(Cart);
    }

    return (
      <div className="blog-article-card" data-id={product.id}>
        <a className="remove-button" href="/#" onClick={handleClick}>
          remove
        </a>
        <img className="checkout-image" src={product.image} alt="store-1" />
        <p className="store-text">{product.name}</p>
        <div className="store-subtitle">$ {product.price}</div>
      </div>
    );
  }
}
