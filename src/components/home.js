import React from 'react';
import '../styles/home.scss';

function Home() {
  return (
    <div>
      <div className="page-wrapper">
        <div className="main-wrapper">
          <div className="top-banner">
            <div className="top-banner-bg" />
            <div className="top-block-title">
              <h1 className="title-big">
                Excel Models <br />
                Built by Investors, for Investors
              </h1>
              <p className="title-description">
                Save time and win more deals with our products and consulting
                services.
              </p>
            </div>
          </div>
          <div className="slider-block">
            <h2 className="title">Testimonials</h2>
            <div className="slider-text">
              "Absolutely loved. I found it to be very lucidly explained, which
              helped me revise the basic modelling skills at a great pace. The
              drawings and quotes were the cherry on top!"
              <div className="slider-author">
                Esha Puri, Associate, Deutsche Bank
              </div>
            </div>
          </div>

          <section className="models">
            <div className="container">
              <h2 className="title">Models</h2>
              <div className="models-block">
                <div className="model-card">
                  <img src="/img/model-1.png" alt="model-1" />
                  <div className="subtitle">Standard Acquisition Model</div>
                  <p className="text">
                    Multifamily Modeling’s basic apartment acquisition model for
                    an existing building. Logical, straightforward, and easy to
                    use, whether you’re new to underwriting or an experienced
                    investor.
                  </p>
                  <a className="button" href="/#">
                    Learn more
                  </a>
                </div>
                <div className="model-card">
                  <img src="../img/model-2.png" alt="model-2" />
                  <div className="subtitle">Value-Add/Renovation Model</div>
                  <p className="text">
                    Multifamily Modeling’s basic apartment acquisition model for
                    an existing building. Logical, straightforward, and easy to
                    use, whether you’re new to underwriting or an experienced
                    investor.
                  </p>
                  <a className="button" href="/#">
                    Learn more
                  </a>
                </div>
                <div className="model-card">
                  <img src="../img/model-3.png" alt="model-3" />
                  <div className="subtitle">Condo Development Model</div>
                  <p className="text">
                    For the ground-up development of a condominium building or
                    complex. Featuring a robust development schedule, allowing
                    users to easily run a number of construction timing
                    scenarios.
                  </p>
                  <a className="button" href="/#">
                    Learn more
                  </a>
                </div>
                <div className="model-card">
                  <img src="../img/model-4.png" alt="model-4" />
                  <div className="subtitle">Apartment Development Model</div>
                  <p className="text">
                    Our flagship development product featuring everything from a
                    Back-of-the-Envelope analysis (allowing developers to get to
                    a quick Yield-on-Cost) to our sophisticated timing and
                    construction schedules.
                  </p>
                  <a className="button" href="/#">
                    Learn more
                  </a>
                </div>
              </div>
            </div>
          </section>

          <section className="consulting">
            <div className="container">
              <div className="title">Consulting</div>
              <div className="consulting-block">
                <div className="consult-card">
                  <img src="../img/consult-1.png" alt="consult-1" />
                  <div className="subtitle">Custom Models</div>
                  <p className="text">
                    Not all models look the same. Need something you don’t see
                    here? We’ll build it for you.
                  </p>
                </div>
                <div className="consult-card">
                  <img src="../img/consult-2.png" alt="consult-2" />
                  <div className="subtitle">Model Audits</div>
                  <p className="text">
                    Sometimes models just need a little love. We review
                    spreadsheets on behalf of our clients in order to verify
                    formulas and improve functionality.
                  </p>
                </div>
                <div className="consult-card">
                  <img src="../img/consult-3.png" alt="consult-3" />
                  <div className="subtitle">Acqusitions Analysis</div>
                  <p className="text">
                    Do you run lean without the capital to invest in a full-time
                    Acquisition Analyst? Don’t worry, we’ve got you covered.
                  </p>
                </div>
              </div>
              <div className="line" />
              <div className="additional-block">
                <p className="text addtitional-text">
                  At Multifamily Modeling, our aim is to provide our clients
                  with high quality and meticulously honed consulting services
                  to help them get to the right answer.
                </p>
                <a className="button" href="/#">
                  Learn more
                </a>
              </div>
            </div>
          </section>

          <div className="about">
            <div className="about-banner" />
            <div className="about-information">
              <h2 className="title">What We Do</h2>
              <p className="text about-text">
                We create straightforward, sophisticated, and
                aesthetically-pleasing financial models designed for the
                multifamily investment and development community. Our firm also
                offers consulting services to multifamily investors and
                developers throughout the US. See our Consulting page to learn
                more. <br />
                <br />
                To us, building financial models is an exercise in problem
                solving, and we are glad to sweat the details so you don’t have
                to.
              </p>
              <a className="button" href="/#">
                Learn more
              </a>
            </div>
          </div>

          <section className="articles">
            <div className="container">
              <h2 className="title">Articles</h2>
              <div className="articles-block">
                <div className="article-card">
                  <img src="/img/article-1.png" alt="article-1" />
                  <div className="subtitle">
                    Multifamily Ground-Up Development Model
                  </div>
                  <p className="text">
                    The Development Models will appeal to developers both small
                    and large. Meaning, a small 2-3 person shop to a private
                    equity firm.
                  </p>
                </div>
                <div className="article-card">
                  <img src="../img/article-2.png" alt="article-2" />
                  <div className="subtitle">
                    Multifamily Ground-Up Development Model
                  </div>
                  <p className="text">
                    The Development Models will appeal to developers both small
                    and large. Meaning, a small 2-3 person shop to a private
                    equity firm.
                  </p>
                </div>
                <div className="article-card">
                  <img src="../img/article-3.png" alt="article-3" />
                  <div className="subtitle">
                    Multifamily Ground-Up Development Model
                  </div>
                  <p className="text">
                    The Development Models will appeal to developers both small
                    and large. Meaning, a small 2-3 person shop to a private
                    equity firm.
                  </p>
                </div>
                <div className="article-card">
                  <div className="next-button" />
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default Home;
