import React from 'react';
import ProductItem from './productItem';
import '../styles/checkout.scss';
import '../styles/store.scss';

export default class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    let listItems = JSON.parse(localStorage.getItem('cart'));
    fetch('./data/allPages.json', {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
      .then(function(resp) {
        return resp.json();
      })
      .then(data => {
        let Cart = [];
        data.items.forEach(item => {
          for (var purchaseId in listItems) {
            if (item.id === listItems[purchaseId]) {
              Cart.push(item);
            }
          }
        });
        this.setState({ products: Cart });
      });
  }

  updateProducts = newProducts => this.setState({ products: newProducts });

  render() {
    const { products } = this.state;

    return (
      <div className="blog">
        <div className="page-wrapper">
          <div className="main-wrapper">
            <section className="blog-arcticles">
              <div className="container">
                <div className="blog-top-panel">
                  <h2 className="title">Checkout</h2>
                </div>
                <div className="blog-articles-block">
                  {products.map((product, index) => (
                    <ProductItem
                      product={product}
                      key={index}
                      updateProducts={this.updateProducts}
                    />
                  ))}
                </div>
              </div>
            </section>

            <div className="pay">
              <a className="pay-button" href="/checkout" onClick={payNow}>
                pay now
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function payNow(e) {
  e.preventDefault();
  let cartData = [];
  setTimeout(() => {
    localStorage.setItem('cart', JSON.stringify(cartData));
    alert('Вы успешно оплатили ваши покупки');
  }, 3000);

  // ProductList.render();
}
