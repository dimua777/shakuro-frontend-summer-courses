import React from 'react';
import '../styles/styles.scss';

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="footer-content">
          <a className="footer-logo" href="index.html">
            <p className="footer-logo-icon" />
            <p className="footer-logo-name">PROJECT NAME</p>
          </a>
          <nav>
            <ul>
              <li>
                <a href="/#" className="link">
                  MODELS
                </a>
              </li>
              <li>
                <a href="/#" className="link">
                  CONSULTING
                </a>
              </li>
              <li>
                <a href="/#" className="link">
                  ABOUT
                </a>
              </li>
              <li>
                <a href="/#" className="link">
                  BLOG
                </a>
              </li>
              <li>
                <a href="/#" className="link">
                  LEGAL
                </a>
              </li>
            </ul>
            <ul>
              <li>
                <img
                  src="../img/place-icon.svg"
                  alt="place-icon"
                  className="icon"
                  href="/#"
                />
                <a className="link" href="/#">
                  P.O. Box XXXX, Fairfield, CT 06815
                </a>
              </li>
              <li>
                <img
                  src="../img/phone-icon.svg"
                  alt="phone-icon"
                  className="icon"
                  href="/#"
                />
                <a className="link" href="/#">
                  (203) xxx-5555
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
