import React from 'react';
import '../styles/blog.scss';

function Blog() {
  return (
    <div className="blog">
      <div className="page-wrapper">
        <div className="main-wrapper">
          <section className="blog-arcticles">
            <div className="container">
              <div className="blog-top-panel">
                <h2 className="title">Articles</h2>
                <input className="blog-search" placeholder="Search" />
              </div>
              <div className="blog-articles-block">
                <div className="blog-article-card">
                  <a href="/#">
                    <img
                      className="blog-arcticle-image"
                      src="../img/blog-1.png"
                      alt="blog-1"
                    />
                    <div className="blog-card-text-wrapper">
                      <div className="blog-subtitle">
                        Multifamily Ground-Up Development Model
                      </div>
                      <p className="blog-text">
                        The Development Models will appeal to developers both
                        small and large. Meaning, a small 2-3 person shop to a
                        private equity firm.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="blog-article-card">
                  <a href="/#">
                    <img
                      className="blog-arcticle-image"
                      src="../img/blog-2.png"
                      alt="blog-2"
                    />
                    <div className="blog-card-text-wrapper">
                      <div className="blog-subtitle">
                        Multifamily Ground-Up Development Model
                      </div>
                      <p className="blog-text">
                        The Development Models will appeal to developers both
                        small and large. Meaning, a small 2-3 person shop to a
                        private equity firm.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="blog-article-card">
                  <a href="/#">
                    <img
                      className="blog-arcticle-image"
                      src="../img/blog-3.png"
                      alt="blog-3"
                    />
                    <div className="blog-card-text-wrapper">
                      <div className="blog-subtitle">
                        Multifamily Ground-Up Development Model
                      </div>
                      <p className="blog-text">
                        The Development Models will appeal to developers both
                        small and large. Meaning, a small 2-3 person shop to a
                        private equity firm.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="blog-article-card">
                  <a href="/#">
                    <img
                      className="blog-arcticle-image"
                      src="../img/blog-3.png"
                      alt="blog-3"
                    />
                    <div className="blog-card-text-wrapper">
                      <div className="blog-subtitle">
                        Multifamily Ground-Up Development Model
                      </div>
                      <p className="blog-text">
                        The Development Models will appeal to developers both
                        small and large. Meaning, a small 2-3 person shop to a
                        private equity firm.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="blog-article-card">
                  <a href="/#">
                    <img
                      className="blog-arcticle-image"
                      src="/img/blog-1.png"
                      alt="blog-1"
                    />
                    <div className="blog-card-text-wrapper">
                      <div className="blog-subtitle">
                        Multifamily Ground-Up Development Model
                      </div>
                      <p className="blog-text">
                        The Development Models will appeal to developers both
                        small and large. Meaning, a small 2-3 person shop to a
                        private equity firm.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="blog-article-card">
                  <a href="/#">
                    <img
                      className="blog-arcticle-image"
                      src="../img/blog-2.png"
                      alt="blog-2"
                    />
                    <div className="blog-card-text-wrapper">
                      <div className="blog-subtitle">
                        Multifamily Ground-Up Development Model
                      </div>
                      <p className="blog-text">
                        The Development Models will appeal to developers both
                        small and large. Meaning, a small 2-3 person shop to a
                        private equity firm.
                      </p>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default Blog;
